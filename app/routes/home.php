<?php
$app->any('/', function($request, $response) use ($app) {
    $db = $app->getContainer()['db'];
    $fetchHome = $db->prepare("SELECT *
        FROM  `entries`
        WHERE  `visible` =1
        ORDER BY  `posted` DESC
        LIMIT 15");
    $fetchHome->execute();
    $homePosts = $fetchHome->fetchAll(PDO::FETCH_ASSOC);
    $params = [
        'posts' => $homePosts
    ];

    if (isset($_SESSION['user'])) $params['user'] = $_SESSION['user'];

    return $this->view->render($response, 'home.twig', $params);
})->setName('home');
