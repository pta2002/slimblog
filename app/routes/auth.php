<?php
$app->get('/register', function($request, $response) use ($app) {
    if (!isset($_SESSION['user']))
        return $this->view->render($response, 'register.twig');
    else
        return $this->view->render($response, 'loggedin.twig', ['user' => $user]);
})->setName('register');

$app->post('/register', function($request, $response) use ($app) {
    if (!isset($_SESSION['user'])) {
        $db = $app->getContainer()['db'];
        $postData = $request->getParsedBody();
        $errors = array();
        if (isset($postData['username']) && isset($postData['password']) &&
        isset($postData['passwordconfirm'])) {
            $username = $postData['username'];
            $password = $postData['password'];
            $passwordConfirm = $postData['passwordconfirm'];
            if (empty($username) || empty($password) ||
            empty($passwordConfirm)) {
                array_push($errors, "All fields are required");
            } else {
                $usernameExists = $db->prepare("SELECT * FROM `users`
                    WHERE `username` = :username");
                $usernameExists->execute(['username'=>$username]);
                if (count($usernameExists->fetchAll(PDO::FETCH_ASSOC)) != 0) {
                    array_push($errors, "That username is already in use.");
                }

                if (!ctype_alnum($username)) {
                    array_push($errors, "Username can only contain letters and numbers.");
                }

                if ($password != $passwordConfirm) {
                    array_push($errors, "Passwords don't match!");
                }

                if (strlen($password) < 8) {
                    array_push($errors, "Password too short!");
                }

                if (strlen($username) > 16) {
                    array_push($errors, "Username too long.");
                }
            }
        } else {
            array_push($errors, "All fields are required");
        }

        if (count($errors) == 0) {
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $insert = $db->prepare("INSERT INTO `users` (`username`, `password`) VALUES (:username, :password)");
            $insert->execute([
                'username' => $username,
                'password' => $hash
            ]);
            return $this->view->render($response, 'success.twig');
        } else {
            return $this->view->render($response, 'register.twig', [
                'errors' => $errors,
                'username' => $username
            ]);
        }
    } else {
        return $this->view->render($response, 'loggedin.twig', ['user' => $user]);
    }
});

$app->get('/login', function ($request, $response) use ($app) {
    if (!isset($_SESSION['user'])) {
        return $this->view->render($response, 'login.twig');
    } else {
        return $this->view->render($response, 'loggedin.twig', ['user' => $user]);
    }
})->setName('login');

$app->post('/login', function($request, $response) use ($app) {
    if (!isset($_SESSION['user'])) {
        $errors = array();
        $db = $app->getContainer()['db'];
        $postData = $request->getParsedBody();

        if (isset($postData['username']) && isset($postData['password'])) {
            $username = $postData['username'];
            $password = $postData['password'];

            if (empty($username) || empty($password)) {
                array_push($errors, "All fields are requried.");
            } else {
                $getInfo = $db->prepare("SELECT * FROM `users` WHERE `username` = :username");
                $getInfo->execute(['username' => $username]);

                $info = $getInfo->fetch(PDO::FETCH_ASSOC);
                if ($info) {
                    if (password_verify($password, $info['password'])) {
                        echo 'done';
                    } else {
                        array_push($errors, "Password is incorrect.");
                    }
                } else {
                    array_push($errors, "User not found.");
                }
            }

        } else {
            array_push($errors, "All fields are requried.");
        }

        if ($errors) {
            return $this->view->render($response, 'login.twig', [
                'username' => $username,
                'errors' => $errors
            ]);
        } else {
            $_SESSION['user'] = new User($info['userid'], $db);
            return $response->withRedirect('/');
        }
    } else {
        return $this->view->render($response, 'loggedin.twig', ['user' => $user]);
    }
});

$app->get('/logout', function ($request, $response) use ($app) {
    session_destroy();
    return $response->withRedirect('/');
})->setName('logout');
