<?php
$app->get('/post/{permalink}', function($request, $response) use ($app) {
    $permalink = $request->getAttribute('permalink');
    $db = $app->getContainer()['db'];
    $searchPermalink = $db->prepare("SELECT * FROM `entries`
    WHERE `permalink` = :permalink AND `visible` != 0");

    $searchPermalink->execute([
        'permalink' => $permalink,
    ]);
    $searchPermalinkResults = $searchPermalink->fetch(PDO::FETCH_ASSOC);
    if ($searchPermalinkResults) {
        $params = [
            'post' => $searchPermalinkResults
        ];
        if (isset($_SESSION['user'])) $params['user'] = $_SESSION['user'];
        return $this->view->render($response, 'viewpost.twig', $params);
    } else {
        $searchId = $db->prepare("SELECT * FROM `entries`
        WHERE `postid` = :id AND `visible` = 1");

        $searchId->execute([
            'id' => $permalink,
        ]);
        $searchIdResults = $searchId->fetch(PDO::FETCH_ASSOC);

        if ($searchIdResults) {
            $params = ['post'=>$searchIdResults];
            if (isset($_SESSION['user'])) $params['user'] = $_SESSION['user'];
            return $this->view->render($response, 'viewpost.twig', $params);
        } else {
            $params = array();
            if (isset($_SESSION['user'])) $params['user'] = $_SESSION['user'];
            return $this->view->render($response, '404.twig', $params)->withStatus(404);
        }
    }
})->setName('viewpost');
