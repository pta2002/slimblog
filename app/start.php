<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../vendor/autoload.php');
require('settings.php');

$app = new \Slim\App(['settings' => $settings]);

$container = $app->getContainer();

$container['view'] = function($container) {
    $view = new \Slim\Views\Twig('../app/views', [
        'cache' => false
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    return $view;
};

use Slim\Handlers\NotFound;
use Slim\Views\Twig;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class NotFoundHandler extends NotFound {

    private $view;
    private $templateFile;

    public function __construct(Twig $view, $templateFile) {
        $this->view = $view;
        $this->templateFile = $templateFile;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) {
        parent::__invoke($request, $response);

        $this->view->render($response, $this->templateFile);

        return $response->withStatus(404);
    }

}

$container['notFoundHandler'] = function ($c) {
    return new NotFoundHandler($c->get('view'), '404.twig', function ($request, $response) use ($c) {
        return $c['response']->withStatus(404);
    });
};

$container['db'] = function($c) use ($settings) {
    $db = new PDO('mysql:host=' . $settings['db']['host'] . ';dbname=' .
                $settings['db']['database'] . ';charset=utf8mb4',
                $settings['db']['user'], $settings['db']['pass']);
    return $db;
};

foreach (glob("../app/routes/*.php") as $filename) {
    include $filename;
}

// TODO: Move to different file
class User {
    public $username;
    public $hash;
    public $userid;
    public $role;
    public $name;
    public $banned;
    public $userinfo;

    function __construct($id, $db) {
        $findUser = $db->prepare("SELECT * FROM `users`
            WHERE `userid` = :userid");
        $findUser->execute(['userid' => $id]);
        if ($info = $findUser->fetch(PDO::FETCH_ASSOC)) {
            $this->username = $info['username'];
            $this->userid = $id;
            $this->hash = $info['hash'];
            $this->role = $info['role'];
            $this->name = $info['name'];
            $this->banned = $info['banned'];
            $this->userinfo = $info['info'];
        }
    }
}

session_start();
$app->run();
?>
