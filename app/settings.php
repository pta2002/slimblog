<?php
$settings = [
    'db' => [
        'host' => '127.0.0.1',
        'user' => 'root',
        'pass' => 'root',
        'database' => 'blog',
    ],
    'displayErrorDetails' => true
];
